const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss')

mix.js('src/js/app.js', './wp-content/themes/realium/assets/js/')
    .sass('src/scss/style.scss', './wp-content/themes/realium/')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    })

