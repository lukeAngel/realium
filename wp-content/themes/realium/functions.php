<?php

require_once('inc/functions/ajax.php');
require_once('inc/functions/custom_nav_menu.php');
require_once('inc/functions/custom_posts.php');
require_once('inc/functions/custom_taxonomies.php');
require_once('inc/functions/enqueue.php');
require_once('inc/functions/theme_setup.php');
require_once('inc/functions/api/api.php');

add_action('after_setup_theme', 'theme_setup'); //theme_setup.php
add_action('admin_menu', 'add_theme_options'); //theme_setup.php
add_action('get_header', 'realium_register_scripts'); //enqueue.php
add_action('wp_enqueue_scripts', 'realium_enqueue_scripts'); //enqueue.php
add_action('init', 'create_custom_taxonomies'); //custom_taxonomies.php
add_action('init', 'create_custom_posts'); //custom_posts.php

add_action('init', function () {
    register_nav_menu('realium_nav', __('Primary Menu', 'realium'));
});
add_action('init', function () {
    register_nav_menu('realium_nav_footer', __('Primary Menu Footer', 'realium'));
});
//add new menu for theme-options page with page callback theme-options-page.

