<?php get_header(); ?>
  <div class="container py-4">

      <?php
      global $post;

      if (have_posts()) {
      $i = 0;

      while (have_posts()) {
      $gallery = get_field('galleria_immagini');
      $video = get_field('link_video');

      ?>


    <h1 class="mb-3"><?php echo the_title(); ?></h1>

      <?php

      the_post();

      $thumbnail = get_the_post_thumbnail_url($post, 'realium-thumbnail');
      if($thumbnail){?>
        <div class="splash-page position-relative">

          <!-- Slider main container -->
          <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
              <!-- Slides -->
              <div class="swiper-slide" style="width: 600px;">
                <img class="w-100 m-auto" src="<?php echo $thumbnail ?>" alt="">
              </div>
              <div class="swiper-slide" style="width: 600px;">
                <div class="aspect-ratio">

<!--                  <iframe src="https://www.youtube.com/embed/--><?php //echo $video ?><!--?rel=0" frameborder="0" allow="accelerometer; autoplay;" allowfullscreen></iframe>-->
                  <?php echo do_shortcode('[embedyt]http://www.youtube.com/watch?v='.$video.'[/embedyt]') ?>
                </div>
              </div>
              <?php
              if($gallery)
              foreach($gallery as $picture){ ?>
                <div class="swiper-slide" style="width: 600px;">
                  <img class="w-100 m-auto" src="<?php echo wp_get_attachment_url($picture) ?>" alt="">
                </div>
              <?php } ?>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>

            <!-- If we need navigation buttons -->

            <!-- If we need scrollbar -->
            <div class="swiper-scrollbar"></div>
          </div>
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>





        </div>
          <?php
      }

      $images = get_field('foto');
      $size = 'realium-medium-h'; // (thumbnail, medium, large, full or custom size)
      if( $images ): ?>
        <ul class="py-2 no-gutters d-flex align-items-center justify-content-between">
            <?php foreach( $images as $image_id ): ?>
              <li class="col-md-5">
                  <?php echo wp_get_attachment_image($image_id['id'], $size ); ?>
              </li>
            <?php endforeach; ?>
        </ul>
      <?php endif; ?>
    <div class="content py-3 my-3">
        <?php
        the_content();
        // get_template_part('template-parts/content', get_post_type());
        }
        }
        ?>
    </div>
    <!-- Preloader -->
    <div id="page-loading-blocs-notifaction" class="page-preloader"></div>
    <!-- Preloader END -->

  </div>
  <script>
    $(document).ready(function(){
        initializeGallerySwiper()
    })
  </script>
<?php get_footer();
