<?php get_header(); ?>
<?php echo $post->content; ?>
<?php echo get_template_part('template-parts/homepage', 'section-prices') ?>
<?php echo get_template_part('template-parts/homepage', 'section-hw') ?>
<?php echo get_template_part('template-parts/homepage', 'grid-videogames') ?>
<?php
if (have_rows('sezione_prezzi')) { ?>
    <div id="sezione-prezzi" class="text-center">
        <div class="mt-3">

            <img class="svg e" src="<?php echo get_template_directory_uri() ?>/assets/svg/e-alium-yellow.svg" alt="">
            <img class="svg a" src="<?php echo get_template_directory_uri() ?>/assets/svg/a-lium-yellow.svg" alt="">

            <ul>

                <?php while (have_rows('sezione_prezzi')) {
                        the_row();
                        ?>
                    <li class="mx-0">

                        <div class="titolo-prezzo">
                            <h4><?php echo get_sub_field('titolo_sezione') ?></h4>
                        </div>
                        <div class="img-sezione"><?php echo wp_get_attachment_image(get_sub_field('immagine_sezione'), 'full') ?></div>
                        <?php
                                if (have_rows('descrizione_prezzi_sezione')) { ?>
                            <div class="info-container align sm:flex item-center">

                                <?php while (have_rows('descrizione_prezzi_sezione')) {
                                                the_row(); ?>
                                    <div class="xs:w-100 sm:w-1/2 md:w-1/4 pacchetto">
                                        <p><?php echo get_sub_field('pacchetto') ?></p>
                                    </div>
                                <?php
                                            } ?>
                            </div>
                        <?php }
                                ?>
                    </li>

                <?php
                    } ?>
            </ul>
        </div>

    </div>

<?php }
?>
<!-- Main container -->

<div id="page-loading-blocs-notifaction" class="page-preloader"></div>

<!-- Main container END -->
<?php get_footer();
