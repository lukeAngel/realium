<!DOCTYPE html>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <!-->
<html <?php language_attributes(); ?>>
<!-- <![endif]-->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />


    <meta charset="utf-8">
    <meta name="keywords" content="">
<!--    <meta name="description" content="Sfida i tuoi amici ► 4 postazioni in Realtà Virtuale ► Simulatore di guida Sparco Pro VR ► Flipper e retrogaming. Vieni a fare una nuova esperienza a Genova.">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
<!--    <link rel="apple-touch-icon" sizes="57x57" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-57x57.png">-->
<!--    <link rel="apple-touch-icon" sizes="60x60" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-60x60.png">-->
<!--    <link rel="apple-touch-icon" sizes="72x72" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-72x72.png">-->
<!--    <link rel="apple-touch-icon" sizes="76x76" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-76x76.png">-->
<!--    <link rel="apple-touch-icon" sizes="114x114" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-114x114.png">-->
<!--    <link rel="apple-touch-icon" sizes="120x120" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-120x120.png">-->
<!--    <link rel="apple-touch-icon" sizes="144x144" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-144x144.png">-->
<!--    <link rel="apple-touch-icon" sizes="152x152" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-152x152.png">-->
<!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/apple-icon-180x180.png">-->
<!--    <link rel="icon" type="image/png" sizes="192x192" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/android-icon-192x192.png">-->
<!--    <link rel="icon" type="image/png" sizes="32x32" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/favicon-32x32.png">-->
<!--    <link rel="icon" type="image/png" sizes="96x96" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/favicon-96x96.png">-->
<!--    <link rel="icon" type="image/png" sizes="16x16" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/img/favicon/favicon-16x16.png">-->
<!--    <meta name="msapplication-TileColor" content="#ffffff">-->
<!--    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">-->
    <meta name="theme-color" content="#ffffff">

<!--    <link rel="stylesheet" type="text/css" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/css/bootstrap.min.css?4353">-->
<!--    <link rel="stylesheet" type="text/css" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/css/animate.min.css?7885">-->
<!--    <link rel="stylesheet" type="text/css" href="--><?//= get_template_directory_uri() ?><!--/assets/basetta/css/font-awesome.min.css">-->
<!--    <script src="https://kit.fontawesome.com/9efe66d8b7.js" crossorigin="anonymous"></script>-->

<!--    <link href='https://fonts.googleapis.com/css?family=Ubuntu&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>-->

    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,600,700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166990110-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-166990110-1');
    </script>

</head>

<body <?php body_class('w-100 mx-auto'); ?>>
<div class="overflow-hidden">
  <header class="py-2 d-lg-flex px-4 align-items-center">
    <div class="navigation d-lg-flex w-100 align-items-center">
      <div id="logo" class="col-lg-6 py-3 py-lg-0">
        <a href="<?= home_url() ?>">
          <img src="<?php echo get_field('logo_realium', 'option') ?>" alt="">
        </a>
      </div>
      <div class="col-lg-6 hidden md:block">
        <div class="d-flex align-items-center">
          <div class="nav-menu">
              <?php wp_nav_menu('primary'); ?>
          </div>
          <div class="ml-auto contacts">
            <ul class="d-flex align-items-center">
              <li class="px-4">
                <a href="#prenotazioni-prezzi" id="prenota" class="btn realium-btn realium">Prenota</a>
              </li>
              <li class="hover-floating px-2"><a target="_blank" class="realicon-instagram" href="https://www.instagram.com/realium_vr/"></a></li>
              <li class="hover-floating px-2"><a target="_blank" class="realicon-facebook" href="https://www.facebook.com/Realium-106957174086460/"></a></li>
              <li class="hover-floating px-2"><a target="_blank" class="realicon-tik-tok" href="https://www.tiktok.com/@realiumvr"></a></li>
              <li class="hover-floating px-2"><a target="_blank" class="realicon-whatsapp" href="https://api.whatsapp.com/send?phone=393755906279&text=Ciao Realium!"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>
    <?php if (is_front_page()) : ?>
      <div id="splash" style="background-image: url('<?php echo get_field('immagine_splash', 'option') ?>')">
        <div class="splash-header">
          <img class="w-100 m-auto" src="<?php echo get_field('logo_grande', 'option') ?>" alt="">
          <h3 class="splash-text"><?php echo get_field('testo_splash', 'option'); ?>
          </h3>
        </div>
      </div>
    <?php endif; ?>
  <div class="site-content-container">
