<?php

function theme_setup()
{
    /*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
    load_theme_textdomain('realium');

    add_theme_support('post-thumbnails');

    add_image_size('realium-featured-image', 1920, 1200, false);

    add_image_size('realium-medium-h', 900);
    
    add_image_size('realium-homepage-hw', 900, 900, true);
    
    add_image_size('realium-videogame-little', 350, 180, true);

    add_image_size('realium-videogame-medium', 600, 300, true);

    add_image_size('realium-thumbnail', 99999, 600, false);

    // Set the default content width.
    $GLOBALS['content_width'] = 600;

    // This theme uses wp_nav_menu() in two locations.
    // register_nav_menus(array(
    //     'menu' => __('Primary Menu', 'realium'),
    // ));

    add_action('init', function () {
        register_nav_menu('realium_nav', __('Primary Menu', 'realium'));
    });

    add_action('init', function () {
        register_nav_menu('realium_nav_footer', __('Primary Menu Footer', 'realium'));
    });
    /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
    add_theme_support('html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ));

    // Add theme support for Custom Logo.
    add_theme_support('custom-logo', array(
        'width'       => 250,
        'height'      => 250,
        'flex-width'  => true,
    ));

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');

    /*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
     */
    add_editor_style(array('assets/css/editor-style.css', realium_fonts_url()));
}
function add_theme_options()
{
    function theme_option_page()
    {
        ?>
        <div class="wrap">
            <h1>Custom Theme Options Page</h1>
            <form method="post" action="options.php">
                <?php
                        ?>
            </form>
        </div>
<?php
    }
    add_submenu_page('themes.php', "Realium - Opzioni", "Realium - Opzioni", "manage_options", "theme-options", "theme_option_page");
}

add_post_type_support('page', 'excerpt');


if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Realium - Opzioni',
			'menu_title'	=> 'Realium - Opzioni',
			'menu_slug' 	=> 'realium-options',
			'capability'	=> 'edit_posts',
			'redirect'	=> false
		));
}