<?php

// = Font url
// ===============================================
function realium_fonts_url()
{
    $font_url = add_query_arg('family', 'Montserrat:200,300,400,500,600,700,800|Cardo:400,700&display=swap', "//fonts.googleapis.com/css");
    return $font_url;
}


// = Enqueue scripts & css
// ===============================================
function realium_register_scripts()
{

    global $post;

    $theme = wp_get_theme();
    $theme_version = $theme->get('Version');
    $theme_path = get_template_directory_uri();

    wp_enqueue_style('realium-fonts', realium_fonts_url(), array(), null);

    wp_enqueue_style('realium-style', $theme_path . '/style.css', array('realium-fonts'), $theme_version);

    // wp_register_style('realium-ie', $theme_path . "/assets/css/ie.css", array('realium-fonts', 'realium-style'), $theme_version);

    // wp_register_script('perfect_scrollbar', $theme_path . "/assets/js/perfect-scrollbar.js", false, null);

    $a =

    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $currentPost = get_queried_object();

    $class = $currentPost ? get_class($currentPost) : '';

    $currentPostId = ($class == 'WP_Term') ? $currentPost->term_id : $currentPost->ID;
}



function realium_enqueue_scripts()
{

    $theme = wp_get_theme();
    $theme_version = $theme->get('Version');
    $theme_path = get_template_directory_uri();
    /**
     * Load our IE-only stylesheet for all versions of IE:
     * <!--[if IE]> ... <![endif]-->
     */
    wp_deregister_script('jquery');
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://code.jquery.com/jquery-2.2.0.min.js", false, null);

    wp_register_script('realium-script', $theme_path . '/assets/js/app.js', array('jquery'), $theme_version, false);
    wp_enqueue_script('jquery');
    $a = wp_enqueue_script('realium-script');
}
