<?php

function create_custom_taxonomies()
{

    create_categoria_taxonomy();
}

function create_categoria_taxonomy()
{

    $taxonomy = 'categoria';

    register_taxonomy(
        $taxonomy, // register custom taxonomy - category
        array('categoria', 'post', 'page'),
        array(
            'hierarchical' => true,
            'labels' => array(
                'name' => 'Categorie',
                'singular_name' => 'Categoria',
            ),
            'capabilities'       => array(
                'manage_terms' => 'manage_categories',
                'edit_terms' => 'manage_categories',
                'delete_terms' => 'manage_categories',
                'assign_terms' => 'read',
            ),
            'show_in_rest' => true,

        )
    );
}
