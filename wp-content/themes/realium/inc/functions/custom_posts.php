<?php

function create_custom_posts()
{

    create_videogame_type();
}
function create_videogame_type()
{

    $labels = array(
        'name' => 'Videogame',
        'singular_name' => 'videogame',
        'add_new' => 'Aggiungi videogame',
        'all_items' => 'Tutti i videogame',
        'add_new_item' => 'Aggiungi videogame',
        'edit_item' => 'Modifica videogame',
        'new_item' => 'Nuovo videogame',
        'view_item' => 'Visualizza videogame',
        'search_items' => 'Cerca videogame',
        'not_found' => 'Nessun videogame trovato',
        'not_found_in_trash' => 'Nessun videogame trovato nel cestino',
        'parent_item_colon' => 'Genitore del videogame'
        //'menu_name' => default to 'name'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'show_in_rest' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'revisions',
            'page-attributes'
        ),
        'taxonomies' => array('categoria'), // add default post categories and tags
        'menu_position' => 5,
        'exclude_from_search' => false,
        // 'register_meta_box_cb' => 'quote_add_post_type_metabox'
    );

    register_post_type('videogame', $args);
}
