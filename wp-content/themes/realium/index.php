<?php get_header(); ?>

<?php
if (have_posts()) {
    $i = 0;

    while (have_posts()) {

        the_post();
        
        get_template_part('template-parts/content', get_post_type());
    }
}
?>
<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->

<?php get_footer();
