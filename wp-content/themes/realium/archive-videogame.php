<?php get_header(); ?>
    <div class="container-fluid py-4">

        <?php
        global $post;?>

        <h1 class="mb-3">Videogames</h1>
        <?php
        if (have_posts()) {
        $i = 0;?>

        <div class="row align-items-stretch">

        <?php
        while (have_posts()) {
          the_post(); ?>

          <div class="col-videogame col-lg-4">
            <a href="<?php echo the_permalink()?>">
              <div class="videogame-container">
                <h4 class="mb-3 text-center"><?php echo the_title(); ?></h4>


                <?php

                $thumbnail = get_the_post_thumbnail_url($post, 'realium-thumbnail');
                if($thumbnail){?>
                    <div class="splash-page">
                        <img class="w-100 m-auto" src="<?php echo $thumbnail ?>" alt="">
                    </div>
                    <?php
                }
                ?>
<!--                <div class="content py-3 my-3">-->
<!--                    --><?php //the_content(); ?>
<!--                </div>-->
              </div>
            </a>
          </div>
   <?php }?>
        </div>
  <?php }
    ?>
            <!-- Preloader -->
            <div id="page-loading-blocs-notifaction" class="page-preloader"></div>
            <!-- Preloader END -->
    </div>
<?php get_footer();
