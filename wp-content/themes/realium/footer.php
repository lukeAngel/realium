<footer class="position-relative">
    <img class="svg e" src="<?php echo get_template_directory_uri() ?>/assets/svg/e-alium.svg" alt="">
    <img class="svg a" src="<?php echo get_template_directory_uri() ?>/assets/svg/a-lium.svg" alt="">
<!--    <div class="pre-footer">-->
<!--      <div class="container">-->
<!--        --><?php //echo do_shortcode('[yikes-mailchimp form="1"]'); ?>
<!--      </div>-->
<!--    </div>-->
    <div class="container text-center text-lg-left">
        <div class="m-auto d-lg-flex py-4">


          <div id="map_84" class="py-4 mb-3 text-center col-12 col-lg-6">
                <div class="">
<!--                  <div class='media-container'><div class='embed-responsive embed-responsive-16by9'><iframe src='https://www.uwp.is.ed.ac.uk/3rd-party-widgets/maps/v4/embeds/gm.php?map=8thsknyQ4j' frameborder='0' class='uwpgmap'></iframe></div></div>-->


                  <iframe class="m-auto" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11401.09923458565!2d8.9420933!3d44.4070063!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xefda77b61a6ad671!2sRealium%20VR!5e0!3m2!1sit!2sit!4v1578151155600!5m2!1sit!2sit" width="400" height="400" frameborder="0" style="border:0;" allowfullscreen="" class="lazyload"></iframe>
                </div>
            </div>
            <div id="info" class="col-12 ml-4 pl-4 py-2 col-lg-6">
                <h3 class="mb-3">Orari</h3>
                <div class="text-left"">
                    <?php echo get_field('informazioni_footer', 'option') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="position-relative contacts d-sm-flex w-100 py-4 px-4">

        <div class="notes hidden w-sm-75"></div>
        <div class="col-sm-12 block">
            <div class="d-flex">
                <div class="ml-auto contacts">
                    <ul class="d-flex align-items-center justify-content-end">
                        <li class="px-8">
                            <button id="prenota" class="btn realium-btn realium ">Prenota</button>
                        </li>
                        <li class="hover-floating px-2"><a target="_blank" class="realicon-instagram" href="https://www.instagram.com/realium_vr/"></a></li>
                        <li class="hover-floating px-2"><a target="_blank" class="realicon-facebook" href="https://www.facebook.com/Realium-106957174086460/"></a></li>
                        <li class="hover-floating px-2"><a target="_blank" class="realicon-tik-tok" href="https://www.tiktok.com/@realiumvr"></a></li>
                        <li class="hover-floating px-2"><a target="_blank" class="realicon-whatsapp" href="https://api.whatsapp.com/send?phone=393755906279&text=Ciao Realium!"></a></li>
                    </ul>
                    <div>
                        <a href="https://www.iubenda.com/termini-e-condizioni/17579479" class="iubenda-black iubenda-embed" title="Termini e Condizioni "><small>Termini e Condizioni</small></a>
                        <small><a href="https://www.iubenda.com/privacy-policy/17579479" class="iubenda-black iubenda-embed" title="Privacy Policy ">Privacy Policy</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
</div>
</body>

</html>