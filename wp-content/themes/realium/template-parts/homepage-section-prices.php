<div id="prenotazioni-prezzi">
    <img class="svg e" src="<?php echo get_template_directory_uri() ?>/assets/svg/e-alium.svg" alt="">
    <div class="container-fluid items-center">
        <div class="prenotazioni text-center">
            <h4>Prenota subito la tua esperienza:</h4>
            <div class="buttons py-4">
                <a target="_blank" href="https://api.whatsapp.com/send?phone=393755906279&text=Ciao%20Realium!" class="btn my-4 realium-btn"><span>Scrivici</span><span class="icon realicon-whatsapp"></span></a>
                <a href="" class="my-4 btn realium-btn"><span>Chiama</span><span class="icon realicon-telephone"></span></a>
            </div>
        </div>
        <div class="prezzi text-center">
            <a href="#prezzi">Scopri i nostri prezzi</a>
        </div>
    </div>
    <img class="svg a" src="<?php echo get_template_directory_uri() ?>/assets/svg/a-lium.svg" alt="">
</div>