<?php

$args = [
    'post_type' => 'videogame',
    'post_status' => 'publish',
    'posts_per_page' => 9,
    'orderby' => 'post_title',
    'order'    => 'ASC',
];


$videogames = new WP_Query($args);
?>

<div class="w-100">
    <ul class="flex">
        <?php foreach($videogames->posts as $single_post): ?>
        <li class="w-1/3 p-4">
            <h3 class="text-center py-2"><?php echo $single_post->post_title ?></h3>
            <img class="w-100" src="<?php  echo wp_get_attachment_image_src(get_post_thumbnail_id($single_post->ID), 'realium-videogame-medium')[0] ?>" alt="">
            <p class="text-center p-2 w-100 videogame-description"><?php echo wp_strip_all_tags($single_post->post_content); ?></p>
        </li>
        <?php endforeach; ?>
    </ul>
</div>