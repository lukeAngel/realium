<div id="section-hw" class="container-fluid">
    <img class="svg e" src="<?php echo get_template_directory_uri() ?>/assets/svg/e-alium-yellow.svg" alt="">
    <img class="svg a" src="<?php echo get_template_directory_uri() ?>/assets/svg/a-lium-yellow.svg" alt="">

    <h3>Le nostre sale</h3>
    <?php
    if (have_rows('dispositivo', 'option')) {
        $count = 1;?>
  <div class="section-container container no-gutters">


<?php
        while (have_rows('dispositivo', 'option')) {
            $count++;
            the_row();
            if(get_sub_field('visibilita')):?>
              <div class="viewer-row d-lg-flex">
                <div class="sx viewer-row-content">
                  <div class="viewer-ratio">
                    <div class="viewer">
                      <?php if ($count % 2 == 0) : ?>
                          <div class="img-container">
                              <?php echo wp_get_attachment_image(get_sub_field('immagine_hw'), 'realium-homepage-hw'); ?>
                          </div>
                      <?php else : ?>
                          <div class="info">
                              <h5><?php echo get_sub_field('nome_dispositivo', 'option') ?></h5>
                              <p><?php echo get_sub_field('informazioni_hw') ?></p>
                          </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <div class="dx viewer-row-content">
                  <div class="viewer">
                    <?php if ($count % 2 != 0) : ?>
                        <div class="img-container">
                            <?php echo wp_get_attachment_image(get_sub_field('immagine_hw'), 'realium-homepage-hw'); ?>
                        </div>
                    <?php else : ?>
                        <div class="info">
                            <h5><?php echo get_sub_field('nome_dispositivo', 'option') ?></h5>
                            <p><?php echo get_sub_field('informazioni_hw') ?></p>
                        </div>
                    <?php endif; ?>

                  </div>
                </div>
              </div>


    <?php endif;
        } ?>
  </div>

  <?php  } ?>

  <div class="container">
  </div>
</div>