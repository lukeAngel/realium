<!-- Main container -->
<div class="page-container">

    <!-- bloc-0 -->
    <div class="bloc bloc-fill-screen b-parallax bg-realium-back1 l-bloc" id="bloc-0">
        <div class="container fill-bloc-top-edge">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-light row navbar-expand-md" role="navigation">
                        <a class="navbar-brand" href="#"><img src="<?= get_template_directory_uri() ?>/assets/basetta/img/realium_scrittasmall.png" alt="RealiumVR-logo" /></a>
                        <button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0 mr-md-0 ml-auto" data-toggle="collapse" data-target=".navbar-1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="mt-5 mt-md-0 navbar-collapse navbar-1">
                            <ul class="site-navigation nav navbar-nav ml-auto">
                                <li class="nav-item text-center">
                                    <a href="tel:3755906279" class="nav-link">Prenota subito al&nbsp;375.5906279</a>
                                </li>
                                <li class="nav-item whatsapp pr-2 text-center">
                                    <a target="_blank" class="nav-link" href="https://api.whatsapp.com/send?phone=393755906279&text=Buongiorno,%20vorrei%20qualche%20informazione%20sulla%20vostra%20sala%20giochi%20." alt="Scrivici con Whatsapp al 375.5906279"><i style="vertical-align: middle;" class="px-2 fa-2x fab fa-whatsapp"></i>Scrivici su Whatsapp</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center col-md-7 offset-lg-1">
                    <h1 id="big_title" class="mg-md text-center text-md-left h1-1-style">
                        Vieni a provare di persona un'esperienza in Realtà Virtuale e sfida i tuoi amici. <br>
                    </h1>
                </div>

                <div class="col-lg-5 col-sm-8 offset-sm-2 offset-md-0 col-md-5 order-md-0 order-1 offset-lg-0">
                    <img src="<?= get_template_directory_uri() ?>/assets/basetta/img/realium_logo.png" class="img-fluid mx-auto d-block" />
                </div>
            </div>
        </div>
        <div class="container fill-bloc-bottom-edge">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <a href="#" data-scroll-speed="1000" onclick="scrollToTarget('0',this)"><span class="fa fa-angle-down icon-md animated bounce animDelay08"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-0 END -->

    <!-- bloc-1 -->
    <div class="bloc bgc-black d-bloc" id="bloc-1">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-md-3">
                    <img src="<?= get_template_directory_uri() ?>/assets/basetta/img/lazyload-ph.png" data-src="<?= get_template_directory_uri() ?>/assets/basetta/img/icn_cubo.png" class="img-fluid mx-auto d-block img-style lazyload" />
                    <div>
                        <div class="text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 50" stroke-width="1" stroke="#000000" fill="none" stroke-linecap="round" style="max-width:500px">
                                <path d="M264,25H500"></path>
                                <path d="M0,25H237"></path>
                                <path d="M251,22v7"></path>
                                <path d="M247,22v7"></path>
                                <path d="M255,22v7"></path>
                                <path d="M259,22v7"></path>
                                <path d="M243,22v7"></path>
                            </svg>
                        </div>
                    </div>
                    <h2 class="mg-md text-lg-center h1-style tc-pastel-orange" id="rosa1">
                        4 postazioni realtà virtuale (singole o multiplayer)<br>
                    </h2>
                </div>
                <div class="col-md-3">
                    <img src="<?= get_template_directory_uri() ?>/assets/basetta/img/lazyload-ph.png" data-src="<?= get_template_directory_uri() ?>/assets/basetta/img/icn_cube3.png" class="img-fluid mx-auto d-block lazyload" />
                    <div>
                        <div class="text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 50" stroke-width="1" stroke="#000000" fill="none" stroke-linecap="round" style="max-width:500px">
                                <path d="M264,25H500"></path>
                                <path d="M0,25H237"></path>
                                <path d="M251,22v7"></path>
                                <path d="M247,22v7"></path>
                                <path d="M255,22v7"></path>
                                <path d="M259,22v7"></path>
                                <path d="M243,22v7"></path>
                            </svg>
                        </div>
                    </div>
                    <h2 class="mg-md text-lg-center h3-style tc-pastel-orange " id="verde">
                        Saletta postazione singola/condivisa (fino a 4 giocatori)<br>
                    </h2>
                </div>
                <div class="col-md-3">
                    <img src="<?= get_template_directory_uri() ?>/assets/basetta/img/lazyload-ph.png" data-src="<?= get_template_directory_uri() ?>/assets/basetta/img/icn_volante.png" class="img-fluid mx-auto d-block img-icn-volan-style lazyload" />
                    <div>
                        <div class="text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 50" stroke-width="1" stroke="#000000" fill="none" stroke-linecap="round" style="max-width:500px">
                                <path d="M264,25H500"></path>
                                <path d="M0,25H237"></path>
                                <path d="M251,22v7"></path>
                                <path d="M247,22v7"></path>
                                <path d="M255,22v7"></path>
                                <path d="M259,22v7"></path>
                                <path d="M243,22v7"></path>
                            </svg>
                        </div>
                    </div>
                    <h3 class="mg-md text-lg-center h3-2-style tc-pastel-orange " id="rosso">
                        Simulatore di guida Sparco Pro VR.<br>
                    </h3>
                </div>
                <div class="col-md-3">
                    <img src="<?= get_template_directory_uri() ?>/assets/basetta/img/lazyload-ph.png" data-src="<?= get_template_directory_uri() ?>/assets/basetta/img/icn_retrogaming.png" class="img-fluid mx-auto d-block img-icn-retrogami-style lazyload" />
                    <div>
                        <div class="text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 50" stroke-width="1" stroke="#000000" fill="none" stroke-linecap="round" style="max-width:500px">
                                <path d="M264,25H500"></path>
                                <path d="M0,25H237"></path>
                                <path d="M251,22v7"></path>
                                <path d="M247,22v7"></path>
                                <path d="M255,22v7"></path>
                                <path d="M259,22v7"></path>
                                <path d="M243,22v7"></path>
                            </svg>
                        </div>
                    </div>
                    <h4 class="mg-md text-lg-center h4-style tc-pastel-orange" id="blu">
                        Flipper e retrogaming.<br>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-1 END -->

    <!-- ScrollToTop Button -->
    <a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><span class="fa fa-chevron-up"></span></a>
    <!-- ScrollToTop Button END-->


    <!-- bloc-2 -->
    <div class="bloc bgc-black d-bloc" id="bloc-2">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col">
                    <p class="p-style text-lg-center">
                        Chiama subito per prenotare la tua esperienza VR<br>Orario di apertura <strong>Lun - Sab 13.00/21.00</strong><br>
                    </p>
                    <div class="text-center prenota">
                        <a href="tel:3755906279" class="btn btn-xl btn-sq btn-onyx"><span class="fa fa-phone icon-spacer icon-onyx"></span>PRENOTA SUBITO AL&nbsp;375 5906279<br></a>
                        <br>
                        <ul class="mt-4" style="list-style: none;">
                            <li style="list-style: none;" class="d-inline-block nav-item whatsapp pr-2 text-center">
                                <a target="_blank" class="nav-link" href="https://api.whatsapp.com/send?phone=393755906279&text=Buongiorno,%20vorrei%20qualche%20informazione%20sulla%20vostra%20sala%20giochi%20." alt="Scrivici con Whatsapp al 375.5906279"><i style="vertical-align: middle;" class="px-2 fa-2x fab fa-whatsapp"></i>Scrivici su Whatsapp</a>
                            </li>
                        </ul>

                        <p class="p-2-style">
                            oppure
                        </p><a href="mailto:info@realiumvr.com" class="btn btn-xl btn-onyx">Scrivi a&nbsp;info@realiumvr.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-2 END -->

    <!-- bloc-4 -->
    <div class="footer bloc bgc-onyx d-bloc" id="bloc-4">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-4">
                    <div class="text-center">
                        <a target="_blank" href="https://www.instagram.com/realium_vr/"><i class="fab fa-instagram fa-5x"></i></a>
                    </div>
                </div>
                <div class="col-4">
                    <div class="text-center">
                        <a target="_blank" href="https://www.facebook.com/Realium-106957174086460/"><i class="fab fa-facebook-square fa-5x"></i></a>
                    </div>
                </div>
                <div class="col-4">
                    <div class="text-center">
                        <a target="_blank" href="https://www.tiktok.com/@realiumvr"><img id="tiktok" src="https://img.icons8.com/officel/80/000000/tiktok.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-4 END -->
    <style>
        iframe {
            max-width: 100%;
        }

        li.whatsapp {
            background-color: #4cc359;
            border-radius: 6px;
        }

        li.whatsapp a.nav-link {
            color: #fff !important;
        }

        .footer a {
            color: #b0c1d4 !important;
        }

        @media screen and (max-width: 1024px) {
            #big_title {
                font-size: 32px;
                text-align: left !important;
                padding: 2
            }

            #tiktok {
                width: 98px;
                height: 98px;
            }
        }
    </style>
</div>
<!-- Main container END -->