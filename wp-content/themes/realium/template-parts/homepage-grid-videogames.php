<?php

$args = [
    'post_type' => 'videogame',
    'post_status' => 'publish',
    'posts_per_page' => 6,
    'orderby' => 'order',
    'order'    => 'ASC',
    'meta_key' => 'visibile_in_homepage',
    'meta_value' => true
];


$videogames = new WP_Query($args);
?>

<div id="homepage-grid-videogames" class="w-100 position-relative">
    <h2 class=" py-4 mb-3">Videogames</h2>
    <img class="svg e" src="<?php echo get_template_directory_uri() ?>/assets/svg/e-alium.svg" alt="">
    <img class="svg a" src="<?php echo get_template_directory_uri() ?>/assets/svg/a-lium.svg" alt="">

    <ul class="row">
        <?php foreach ($videogames->posts as $single_post) : ?>
          <li class="col-lg-4 p-4">
            <a href="<?php echo get_permalink($single_post->ID) ?>">
                <img class="w-100" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($single_post->ID), 'realium-videogame-medium')[0] ?>" alt="">
                <h4 class="text-center py-2 m-y3"><?php echo $single_post->post_title ?></h4>

              <!--                <p class="text-center p-2 w-100 videogame-description">--><?php //echo wp_strip_all_tags($single_post->post_content); ?><!--</p>-->
            </a>
          </li>
        <?php endforeach; ?>
    </ul>
  <div class="text-center">
    <a href="/videogame">Vedi tutti i giochi</a>
  </div>
</div>