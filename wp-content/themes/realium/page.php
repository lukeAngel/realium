<?php get_header(); ?>
<div class="container py-4">

<?php
global $post;

if (have_posts()) {
    $i = 0;
    
    while (have_posts()) {?>


<h1 class="mb-3"><?php echo the_title(); ?></h1>

<?php

the_post();
      
$thumbnail = get_the_post_thumbnail_url($post, 'realium-thumbnail');
if($thumbnail){?>
    <div class="splash-page">
        <img class="w-100 m-auto" src="<?php echo $thumbnail ?>" alt="">
    </div>
<?php
}

$images = get_field('foto');
$size = 'realium-medium-h'; // (thumbnail, medium, large, full or custom size)
if( $images ): ?>
    <ul class="py-2 no-gutters d-flex align-items-center justify-content-between">
        <?php foreach( $images as $image_id ): ?>
            <li class="col-md-5">
                <?php echo wp_get_attachment_image($image_id['id'], $size ); ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<div class="content py-3 my-3">
    <?php
    the_content();
            // get_template_part('template-parts/content', get_post_type());
        }
    }
    ?>
</div>
<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->

</div>
<?php get_footer();
